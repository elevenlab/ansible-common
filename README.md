Ansible Role: Common
===================

Provides common prerequisites on Debian servers, it will:

*	Install Common packets on Debian servers.
*	Create roles and group requested
*	Setup Locale and timezone
*	Disable/Uninstall unwanted services

Updating timezone it will restart `cron` and `rsyslog` in order to reflects the change

----------

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	common_packages:
	    - sudo
	    - build-essential
	    - ntp
	    - vim
	    - git
	    - curl
	    - htop
	    - locate
A list of packages to install on the system, in order to add or remove packages from this list you need to override the whole list


	common_users: []

The list of users with group wanted available on the system. The role will create before the group and then the user bound to it. (user will have `/bin/bash` as shell).
By default no user/group will be added to the system

An example could be:
	
	common_users:
		- name: user1
		  group: group
		- name: user2
		  group: group
this will create `user1` and `user2` both in the group `group`


	common_locale:
		- it_IT.UTF-8
		- en_US.UTF-8
The list of locale that you want enabled and available on the system. By default `it_IT.UTF-8`and `en_US.UTF-8` are used


	common_disable_services:
		- exim4
		- rpcbind
		- nfs-common
The list of services that you want to be stopped and disabled from system. It is a good choice to disable a service before uninstall otherwise it will remain in memory untill first reboot


	common_remove_packages:
		- exim4
	    - exim4-base
	    - exim4-config
	    - exim4-daemon-light
	    - exim4-daemon-heavy
	    - rpcbind
	    - nfs-common
List of unwanted packages. These packages will be removed from system


Dependencies
-------------------
None.

Example Playbook
--------------------------
	- name: configure common packages
	  hosts: manager
	  roles:
	    - role:  common
	      common_locale: en_GB.UTF-8
	      common_timezone: Etc/GMT-6
	      common_users:
	          - name: ansible,
	            group: staff, 
